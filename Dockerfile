FROM debian:jessie
MAINTAINER Xavier Martin <nolazybits+docker@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# copy all the scripts to bin
COPY scripts/bin/ /usr/local/bin/
RUN chmod -R +x  /usr/local/bin/*.sh

# install tools (vim, ...)
COPY scripts/01_install_tools.sh /tmp/
RUN chmod +x /tmp/01_install_tools.sh; sync && \
    /tmp/01_install_tools.sh

# install apache
COPY scripts/02_install_apache.sh /tmp/
RUN chmod +x /tmp/02_install_apache.sh; sync && \
    /tmp/02_install_apache.sh

# install php
COPY scripts/03_install_php.sh /tmp/
RUN chmod +x /tmp/03_install_php.sh; sync && \
    /tmp/03_install_php.sh

# copy xdebug config
COPY scripts/config/xdebug.ini /etc/php5/mods-available/xdebug.ini

EXPOSE 80 443

CMD ["apache2-foreground.sh"]
