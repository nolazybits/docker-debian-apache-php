### Docker image with Apache 2.x and PHP 5.x based on Debian Jessie ###
[![](https://images.microbadger.com/badges/image/nolazybits/debian-apache-php.svg)](http://microbadger.com/images/nolazybits/debian-apache-php "See the layers")

[![](https://images.microbadger.com/badges/version/nolazybits/debian-apache-php.svg)](http://microbadger.com/images/nolazybits/debian-apache-php)