#!/usr/bin/env bash

apt-get update
apt-get -y --force-yes install \
        apt-utils \
        net-tools \
        supervisor \
        vim