#!/usr/bin/env bash

apt-get -y --force-yes install \
        libapache2-mod-php5 \
        php-pear \
        php5 \
        php5-curl \
        php5-dev \
        php5-gd \
        php5-imap \
        php5-mysqlnd \
        php5-xdebug \
        php5-json \
        php5-memcached